var express = require('express');
var app = express();
var db = require('./lib/mysql');

module.exports = app;

var api_call = express.Router();

api_call.get('/',function(req,res) {});

api_call.get('/:userName&:userPassword', checkUserLogin, function(req,res) {
	res.json(req.user)
});

api_call.get('/',function(req,res) {});
api_call.get('/appVersion',checkVersion,function(req,res){
	res.json(req.version)
});

setInterval(function () {
    db.query('SELECT 1');
    console.log('\nKeeping SQL Connection Alive!!!');
}, 30000);

app.use('/userLogin',api_call);
app.use('/checkVersion', api_call);

console.log('API registered');

function checkVersion(req,res,next){
	var sql = 'select Application_Version.appVersionNumber, Application_Version.appLink from Application_Version order by appVersionID DESC limit 1';
	db.query(sql,function(err,results){
		console.log('\nExecuting version query');
		if(err){
			console.log(err);
			res.statusCode = 500;
			return res.json({errors : ['There was a problem']})
		}
		if(results.length ===0){
			res.statusCode = 404;
			return res.json({errors : ['Can not found version number']})
		}
		req.version = results;
		next();
	})
}


function checkUserLogin(req, res, next){
	var userName = req.params.userName;
	var userPassword = req.params.userPassword;
	var sql = 'select userTable.userName, userTable.userPhone, userTable.userEmail, subscriptionTable.expiredDate' +
				' from userTable inner join subscriptionTable On subscriptionTable.userID = userTable.userID ' +
				' where userTable.userType = 1' +
				' and userTable.isActive = 1'+
				' and userTable.userName = ?' + 
				' and userTable.userPassword = ?';
	db.query(sql,[userName,userPassword],function(err,results){
		console.log('\nExecuting query');
		console.log('Result set length: '+ results.length);
		if(err){
			console.log(err);
			res.statusCode = 500;
			return res.json({errors: ['There was a problem!!']})
		}
		if(results.length === 0){
		res.statusCode = 404;
		return res.json({errors: ['Not found any user']})
		}
		req.user = results;
		console.log(req.user)
		next();
	})
	

}


